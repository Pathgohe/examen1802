<?php
namespace App\Controllers;
require_once '../app/models/Jugador.php';
require_once '../app/models/Puestos.php';

use \App\Models\Jugador;
use \App\Models\Puestos;

/**
*
*/
class TitularesController
{

    function __construct()
    {

    }

    public function index()
    {
        if(!isset($_SESSION['titulares'])){
            $titulares=$_SESSION['titulares'];
        }
        require "../app/views/jugador/titulares.php";

    }//final del metodo index
    public function anadir($id){
        $id = $id[0];

        $jugador= Jugador::find($id);

        if(!isset($_SESSION['titulares'][$jugador->id])){
            $_SESSION['titulares'][$jugador->id] = $jugador;

        } else {
            $_SESSION['titulares'][$jugador->id]->jugador ++;
        }
        header('Location:/jugador');
    }//final de anadir




}
