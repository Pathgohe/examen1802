<?php

namespace App\Controllers;
require_once '../app/models/Jugador.php';
use \App\Models\Jugador;
use \App\Models\Puestos;
require_once '../app/models/Puestos.php';

class JugadorController{

    function __construct(){

    }

    public function index(){
        $pagesize = 5;
        $jugadores = Jugador::paginate($pagesize);
        $rowCount = Jugador::rowCount();

        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }
        require "../app/views/jugador/index.php";
    }

    public function create(){
        $posiciones = Puestos::all();
        require "../app/views/jugador/nuevo.php";
    }

    public function store(){
        $jugador=new Jugador();
        $jugador->nombre=$_REQUEST['nombre'];
        $jugador->nacimiento=$_REQUEST['nacimiento'];
        $jugador->id_puesto=$_REQUEST['posicion'];
        $jugador->insert();
        header("location:/jugador");
    }//final de store

    public function anadir($id){
        $id = $id[0];

        $jugador= Jugador::find($id);

        if(!isset($_SESSION['titulares'][$jugador->id])){
            $_SESSION['titulares'][$jugador->id] = $jugador;

        } else {
            $_SESSION['titulares'][$jugador->id]->jugador ++;
        }

        header('Location:/jugador');
    }//final de anadir
    public function remove($id){
        $id = $id[0];
        $jugador = Jugador::find($id);

        if(isset($_SESSION['titulares'][$jugador->id])){
            unset($_SESSION['titulares'][$jugador->id]);
        }
        header('Location:/titulares');
    }

}//final de class jugadorController

