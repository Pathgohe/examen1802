<?php
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\Puestos;
require_once '../core/Model.php';
require_once '../app/models/Puestos.php';



/**
*
*/
class Jugador extends Model
{

    function __construct()
    {
        # code...
    }
    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }
public function paginate($size = 10){
        if (isset($_REQUEST['page'])) {
            $page = (integer) $_REQUEST['page'];
        } else {
            $page = 1;
        }
        $offset = ($page - 1) * $size;

        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $jugadores;
    }//final de paginate

    public static function rowCount(){
        $db = Jugador::db();
        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount['count'];
    }//final de rowCount

    public function insert(){
        $db=Jugador::db();
        $stmt = $db->prepare('INSERT into jugadores(nombre, nacimiento, id_puesto) values(:nombre, :nacimiento, :id_puesto)');
        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':nacimiento', $this->nacimiento);
        $stmt->bindValue(':id_puesto', $this->id_puesto);
        return $stmt->execute();
    }//final de insertar

    public function type(){
        $db = Puestos::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
        $stmt->execute(array(':id' => $this->id_puesto));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Puestos::class);
        $puesto = $stmt->fetch(PDO::FETCH_CLASS);

        return $puesto;
    }

    public static function find($id){

        $db=Jugador::db();
        $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
        $stmt->execute(array(':id'=>$id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $jugador=$stmt->fetch(PDO::FETCH_CLASS);

        return $jugador;
    }//final de find







}//final de clase Jugador
