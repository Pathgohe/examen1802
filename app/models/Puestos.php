<?php
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\Jugador;

require_once '../core/Model.php';
require_once '../app/models/Jugador.php';
/**
*
*/
class Puestos extends Model
{

    function __construct()
    {
        # code...
    }
    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }//final de function __get

    public static function all(){

        $db= Puestos::db();
        $statement = $db->query('SELECT * FROM puestos');
        $puestos=$statement->fetchAll(PDO::FETCH_CLASS, Puestos::class);

        return $puestos;

    }//final funcion all

    public function posicion($id_puesto)
    {

        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugador WHERE id_puesto = :id');
         $statement->execute(array(':id' => $id_puesto));
        $statement->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $puestoId = $statement->fetchAll(PDO::FETCH_CLASS);

        return $puestoId;
    }//final de posicion
    public function find($id){
         $db = Puestos::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Puestos::class);
        $id_puestos = $stmt->fetch(PDO::FETCH_CLASS);

        return $id_puestos;
    }
    public function type(){
        $db = Puestos::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id_puesto=:id');
        $stmt->execute(array(':id' => $this->id_puesto));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Puestos::class);
        $puesto_id = $stmt->fetch(PDO::FETCH_CLASS);

        return $puesto_id;
    }




}
