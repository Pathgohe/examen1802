<!doctype html>
<html lang="es">
  <head>
   <?php require "../app/views/parts/head.php" ?>

    <title>Productos</title>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>
<main role="main" class="container">

  <div id="espaciado"></div>

        <h1>Lista de jugadores</h1>
        <a href="/jugador/create">Nuevo Jugador</a>
        <table class="table table-striped">
          <thead>
          <tr>

            <th>Nombre</th>
            <th>Fecha Nacimiento</th>
            <th>Puesto</th>
           </tr>
          </thead>
          <tbody>

            <?php foreach ($jugadores as $jugador): ?>
              <tr>
              <td><?php echo $jugador->nombre ?></td>
              <td><?php echo date('d-m-Y',strtotime($jugador->nacimiento)) ?></td>

              <td><?php echo $jugador->type->nombre?>
              <td><a class="btn btn-primary" href="/jugador/anadir/<?php echo $jugador->id?>">Añadir</a>
            </tr>
            <?php endforeach ?>
          </tbody>
        </table>

        <?php for ($i=1; $i <= $pages; $i++) { ?>
      <?php if ($i != $page): ?>
      <a href="/jugador?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
      <?php else: ?>
        <span class="btn">
        <?php echo $i ?>
        </span>
      <?php endif ?>
    <?php } ?>
    <hr>


      </div>

    </main><!-- /.container -->





<?php require "../app/views/parts/footer.php" ?>

  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
