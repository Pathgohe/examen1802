<!doctype html>
<html lang="es">
  <head>
    <?php require "../app/views/parts/head.php" ?>
  </head>

  <body>
<!--añadimo con esta linea el header-->
  <?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">

    <div id="espaciado"></div>
      <div>
        <h1>Nuevo Jugador: </h1>
      <form method="post" action="/jugador/store">
        <div class="form-group">
        <label>Nombre:</label><input type="text" class="form-control" name="nombre">
        <label>Fecha nacimiento:</label><input type="datetime-local" class="form-control" name="nacimiento">

        <p>Posición:</p>
        <select name="posicion">
          <?php foreach($posiciones as $tipo) {
           ?> <option <?php echo $tipo->id == $tipo->id ? ' "selected"' : '' ?> value="<?php echo $tipo->id ?>">
            <?php echo $tipo->nombre ?>
             </option>
         <?php } ?>



        </select>
        <input type="submit" value="Añadir">
        </div>

      </form>


      </div>

    </main><!-- /.container -->

<?php require "../app/views/parts/footer.php" ?>

</body>

<?php require "../app/views/parts/scripts.php" ?>
</html>
