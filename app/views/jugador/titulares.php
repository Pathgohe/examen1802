<!doctype html>
<html lang="es">
  <head>
   <?php require "../app/views/parts/head.php" ?>

    <title>Titulares</title>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>
<main role="main" class="container">

  <div id="espaciado"></div>

        <h1>Titulares</h1>
        <?php
              if(isset($_SESSION['titulares']) && !empty($_SESSION['titulares'])){ ?>

        <table class="table table-striped">
          <thead>
          <tr>
            <th>Titular Id</th>
            <th>Nombre</th>
            <th>Fecha Nacimiento</th>
            <th>Puesto</th>

           </tr>
          </thead>
          <tbody>


            <?php

            foreach ($_SESSION['titulares'] as $titular) {

           ?>
              <tr>
              <td><?php echo $titular->id ?></td>
              <td><?php echo $titular->nombre ?></td>
              <td><?php echo date('d-m-Y',strtotime($titular->nacimiento)) ?> </td>
              <td><?php echo $titular->type->nombre ?></td>


            <td><a class="btn btn-primary" href="/jugador/remove/<?php echo $titular->id?>">Elimiar</a>
            </tr>
            <?php  } ?>
          </tbody>
        </table>
        <?php }else{
              $_SESSION['titulares']="";
              echo "<p>No hay Titulares en su equipo</p>";
            } ?>
      </div>


    </main><!-- /.container -->

<?php require "../app/views/parts/footer.php" ?>

  </body>
    <?php require "../app/views/parts/scripts.php" ?>
</html>
